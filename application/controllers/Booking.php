<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class booking extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/booking/';
        $this->_path_js = null;
        $this->_judul = 'Booking';
        $this->_controller_name = 'booking';
        $this->_model_name = 'model_booking';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_package'] = $this->{$this->_model_name}->get_ref_table('ref_package');
        $data['ref_guide'] = $this->{$this->_model_name}->get_ref_table('ref_guide');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['bookId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_package'] = $this->{$this->_model_name}->get_ref_table('ref_package');
        $data['ref_guide'] = $this->{$this->_model_name}->get_ref_table('ref_guide');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $bookIdOld = $this->input->post('bookIdOld');
        $this->form_validation->set_rules('bookPackage', 'Package', 'trim|xss_clean');
        $this->form_validation->set_rules('bookGuide', 'Guide', 'trim|xss_clean');
        $this->form_validation->set_rules('bookName', 'Name', 'trim|xss_clean');
        $this->form_validation->set_rules('bookEmail', 'Email', 'trim|xss_clean');
        $this->form_validation->set_rules('bookNumber', 'Phone', 'trim|xss_clean');
        $this->form_validation->set_rules('bookMessage', 'Message', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $bookPackage = $this->input->post('bookPackage');
                $bookGuide = $this->input->post('bookGuide');
                $bookName = $this->input->post('bookName');
                $bookEmail = $this->input->post('bookEmail');
                $bookNumber = $this->input->post('bookNumber');
                $bookMessage = $this->input->post('bookMessage');


                $param = array(
                    'bookPackage' => $bookPackage,
                    'bookGuide' => $bookGuide,
                    'bookName' => $bookName,
                    'bookEmail' => $bookEmail,
                    'bookNumber' => $bookNumber,
                    'bookMessage' => $bookMessage,

                );

                if (empty($bookIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_booking', $param);
                } else {
                    $key = array('bookId' => $bookIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_booking', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['bookId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_booking', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
