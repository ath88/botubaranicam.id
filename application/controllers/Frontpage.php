<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Frontpage extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->_path_page   = 'pages/frontpage/';
		$this->_judul       = '';
		$this->_path_js     = '';
		$this->_controller_name = 'frontpage';
		$this->_model_name  = 'model_home';
		$this->load->model($this->_model_name, '', TRUE);
	}

	public function index()
	{
		$data['slider'] = $this->{$this->_model_name}->rating();
		$data['highlight'] = $this->{$this->_model_name}->get_ref_table('ref_highlight', 'hlId DESC');
		$data['package'] = $this->{$this->_model_name}->get_ref_table('ref_package', 'packName');
		$data['partner'] = $this->{$this->_model_name}->get_ref_table('ref_partner', 'partnerId');
		$data['guided'] = $this->{$this->_model_name}->get_ref_table('ref_guide', 'guideRating DESC');
		$whales = $this->{$this->_model_name}->get_ref_table('ref_sight_whale', 'swDate DESC');
		$w_month = $w_whales = null;
		if ($whales != false) :
			$i = 0;
			foreach ($whales as $row) :
				if ($i < 30) :
					$w_month[] = $row->swDate;
					$w_whales[] = $row->swValue;
				endif;
			endforeach;
			$w_month = array_reverse($w_month);
			$w_whales = array_reverse($w_whales);
		endif;
		$data['w_month'] = $w_month;
		$data['w_whales'] = $w_whales;
		$this->load->view($this->_path_page . 'index', $data);
	}

	public function rating()
	{
		$this->form_validation->set_rules('value', 'Rating', 'trim|xss_clean|required');
		$this->form_validation->set_rules('id', 'Camera ID', 'trim|xss_clean|required');

		if ($this->form_validation->run()) {
			if (IS_AJAX) {
				$rateCamId = $this->input->post('id');
				$rateValue = $this->input->post('value');
				$param = array(
					'rateCamId' => $rateCamId,
					'rateDatetime' => date('Y-m-d H:i:s'),
					'rateIP' => $this->input->ip_address(),
					'rateValue' => $rateValue,
					'rateSession' => $this->session->session_id,
				);

				$check_id = $this->{$this->_model_name}->get_ref_table(
					'ref_cam_rating',
					'',
					"rateIP = '" . $this->input->ip_address() . "' AND DATE(rateDatetime) = '" . date('Y-m-d') . "' AND rateCamId = '" . $rateCamId . "'"
				);
				if ($check_id == false) {
					$proses = $this->{$this->_model_name}->insert('ref_cam_rating', $param);

					if ($proses)
						message('Rating Accepted', 'success');
					else
						message('Rating Failed', 'error');
				} else {
					message('You have already rating today', 'error');
				}
			}
		}
	}

	public function booking()
	{
		$this->form_validation->set_rules('template-contactform-package', 'Package', 'trim|xss_clean');
		$this->form_validation->set_rules('template-contactform-guide', 'Guide', 'trim|xss_clean');
		$this->form_validation->set_rules('template-contactform-name', 'Name', 'trim|xss_clean');
		$this->form_validation->set_rules('template-contactform-email', 'Email', 'trim|xss_clean');
		$this->form_validation->set_rules('template-contactform-phone', 'Phone', 'trim|xss_clean');
		$this->form_validation->set_rules('template-contactform-message', 'Message', 'trim|xss_clean');

		if ($this->form_validation->run()) {
			if (IS_AJAX) {
				$bookPackage = $this->input->post('template-contactform-package');
				$bookGuide = $this->input->post('template-contactform-guide');
				$bookName = $this->input->post('template-contactform-name');
				$bookEmail = $this->input->post('template-contactform-email');
				$bookNumber = $this->input->post('template-contactform-phone');
				$bookMessage = $this->input->post('template-contactform-message');

				$param = array(
					'bookPackage' => $bookPackage,
					'bookGuide' => $bookGuide,
					'bookName' => $bookName,
					'bookEmail' => $bookEmail,
					'bookNumber' => $bookNumber,
					'bookMessage' => $bookMessage,

				);

				$proses = $this->{$this->_model_name}->insert('ref_booking', $param);

				if ($proses)
					message('Thanks for your booking, we will be in touch soon to confirm', 'success');
				else {
					$error = $this->db->error();
					message('Sorry failed to booking', 'error');
				}
			}
		} else {
			message('Ooops!! Something Wrong!! ', 'error');
		}
	}
}
