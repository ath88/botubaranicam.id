<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class guide extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/guide/';
        $this->_path_js = null;
        $this->_judul = 'Guide';
        $this->_controller_name = 'guide';
        $this->_model_name = 'model_guide';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_guide');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['guideId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_guide', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $guideIdOld = $this->input->post('guideIdOld');
        $this->form_validation->set_rules('guideName', 'guideName', 'trim|xss_clean|required');
        $this->form_validation->set_rules('guidePhoto', 'guidePhoto', 'trim|xss_clean|required');
        $this->form_validation->set_rules('guideRating', 'guideRating', 'trim|xss_clean|required');
        $this->form_validation->set_rules('guideFB', 'guideFB', 'trim|xss_clean');
        $this->form_validation->set_rules('guideInstagram', 'guideInstagram', 'trim|xss_clean');
        $this->form_validation->set_rules('guideTwitter', 'guideTwitter', 'trim|xss_clean');
        $this->form_validation->set_rules('guideFitur1', 'guideFitur1', 'trim|xss_clean|required');
        $this->form_validation->set_rules('guideFitur2', 'guideFitur2', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $guideName = $this->input->post('guideName');
                $guidePhoto = $this->input->post('guidePhoto');
                $guideRating = $this->input->post('guideRating');
                $guideFB = $this->input->post('guideFB');
                $guideInstagram = $this->input->post('guideInstagram');
                $guideTwitter = $this->input->post('guideTwitter');
                $guideFitur1 = $this->input->post('guideFitur1');
                $guideFitur2 = $this->input->post('guideFitur2');


                $param = array(
                    'guideName' => $guideName,
                    'guidePhoto' => $guidePhoto,
                    'guideRating' => $guideRating,
                    'guideFB' => $guideFB,
                    'guideInstagram' => $guideInstagram,
                    'guideTwitter' => $guideTwitter,
                    'guideFitur1' => $guideFitur1,
                    'guideFitur2' => $guideFitur2,

                );

                if (empty($guideIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_guide', $param);
                } else {
                    $key = array('guideId' => $guideIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_guide', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['guideId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_guide', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
