
<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class highlight extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/highlight/';
        $this->_path_js = null;
        $this->_judul = 'Highlight';
        $this->_controller_name = 'highlight';
        $this->_model_name = 'model_highlight';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_highlight');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['hlId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_highlight', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $hlIdOld = $this->input->post('hlIdOld');
        $this->form_validation->set_rules('hlTitle', 'Title', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hlCaption', 'Caption', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hlSource', 'Source', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $hlTitle = $this->input->post('hlTitle');
                $hlCaption = $this->input->post('hlCaption');
                $hlSource = $this->input->post('hlSource');

                $param = array(
                    'hlTitle' => $hlTitle,
                    'hlCaption' => $hlCaption,
                    'hlSource' => $hlSource,

                );

                if (empty($hlIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_highlight', $param);
                } else {
                    $key = array('hlId' => $hlIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_highlight', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['hlId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_highlight', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
