<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class package extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/package/';
        $this->_path_js = null;
        $this->_judul = 'Package';
        $this->_controller_name = 'package';
        $this->_model_name = 'model_package';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_package');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['packId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_package', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $packIdOld = $this->input->post('packIdOld');
        $this->form_validation->set_rules('packName', 'Package Name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('packPrice', 'Price', 'trim|xss_clean|required');
        $this->form_validation->set_rules('packFitur1', 'Feature 1', 'trim|xss_clean|required');
        $this->form_validation->set_rules('packFitur2', 'Feature 2', 'trim|xss_clean');
        $this->form_validation->set_rules('packFitur3', 'Feature 3', 'trim|xss_clean');
        $this->form_validation->set_rules('packFitur4', 'Feature 4', 'trim|xss_clean');
        $this->form_validation->set_rules('packFitur5', 'Feature 5', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $packName = $this->input->post('packName');
                $packPrice = $this->input->post('packPrice');
                $packFitur1 = $this->input->post('packFitur1');
                $packFitur2 = $this->input->post('packFitur2');
                $packFitur3 = $this->input->post('packFitur3');
                $packFitur4 = $this->input->post('packFitur4');
                $packFitur5 = $this->input->post('packFitur5');


                $param = array(
                    'packName' => $packName,
                    'packPrice' => $packPrice,
                    'packFitur1' => $packFitur1,
                    'packFitur2' => $packFitur2,
                    'packFitur3' => $packFitur3,
                    'packFitur4' => $packFitur4,
                    'packFitur5' => $packFitur5,

                );

                if (empty($packIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_package', $param);
                } else {
                    $key = array('packId' => $packIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_package', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['packId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_package', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
