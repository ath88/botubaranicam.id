<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class rating extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/rating/';
        $this->_path_js = null;
        $this->_judul = 'Rating';
        $this->_controller_name = 'rating';
        $this->_model_name = 'model_rating';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_camera'] = $this->{$this->_model_name}->get_ref_table('ref_camera');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['rateId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_camera'] = $this->{$this->_model_name}->get_ref_table('ref_camera');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $rateIdOld = $this->input->post('rateIdOld');
        $this->form_validation->set_rules('rateCamId', 'Camera', 'trim|xss_clean|required');
        $this->form_validation->set_rules('rateValue', 'rateValue', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $rateCamId = $this->input->post('rateCamId');
                $rateValue = $this->input->post('rateValue');

                $param = array(
                    'rateCamId' => $rateCamId,
                    'rateDatetime' => date('Y-m-d H:i:s'),
                    'rateIP' => $this->input->ip_address(),
                    'rateValue' => $rateValue,
                    'rateSession' => $this->session->session_id,

                );

                if (empty($rateIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_cam_rating', $param);
                } else {
                    $key = array('rateId' => $rateIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_cam_rating', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['rateId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_cam_rating', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
