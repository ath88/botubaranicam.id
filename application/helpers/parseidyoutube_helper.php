<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('youtubeid'))
{
	function youtubeid($urlyoutube)
	{
		parse_str(parse_url($urlyoutube)['query'], $query);
        return $query['v'];
	    exit();
	}
}
