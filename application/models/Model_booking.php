<?php
class Model_booking extends Model_Master
{
    protected $table = 'ref_booking';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_package', 'bookPackage = packId', 'LEFT');
        $this->db->join('ref_guide', 'bookGuide = guideId', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_package', 'bookPackage = packId', 'LEFT');
        $this->db->join('ref_guide', 'bookGuide = guideId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
