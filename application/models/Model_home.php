<?php
class Model_home extends Model_Master
{
	public function __construct()
	{
		parent::__construct();
	}

	function rating()
	{
		$this->db->select('ref_camera.*,AVG(rateValue) rateValue');
		$this->db->from('ref_camera');
		$this->db->join('ref_cam_rating', 'rateCamId = camId', 'LEFT');
		$this->db->group_by('camId');
		$this->db->order_by('camId');
		$qr = $this->db->get();
		if ($qr->num_rows() > 0)
			return $qr->result();
		else
			return false;
	}
}
