<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="bookIdOld" value="<?= $datas != false ? $datas->bookId : '' ?>">

                        <div class="form-group">
                            <label>Package</label>
                            <select class="form-control m-select2" name="bookPackage">
                                <option value=""></option>
                                <?php
                                foreach ($ref_package as $row) :
                                    echo '<option value="' . $row->packId . '" ' . ($datas != false ? ($row->packId == $datas->bookPackage ? 'selected' : '') : '') . '>' . $row->packName . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Guide</label>
                            <select class="form-control m-select2" name="bookGuide">
                                <option value=""></option>
                                <?php
                                foreach ($ref_guide as $row) :
                                    echo '<option value="' . $row->guideId . '" ' . ($datas != false ? ($row->guideId == $datas->bookGuide ? 'selected' : '') : '') . '>' . $row->guideName . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Reservator</label>
                            <input type="text" class="form-control" name="bookName" placeholder="Reservator" aria-describedby="Reservator" value="<?= $datas != false ? $datas->bookName : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="bookEmail" placeholder="Email" aria-describedby="Email" value="<?= $datas != false ? $datas->bookEmail : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" class="form-control" name="bookNumber" placeholder="Phone" aria-describedby="Phone" value="<?= $datas != false ? $datas->bookNumber : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Message</label>
                            <input type="text" class="form-control" name="bookMessage" placeholder="Message" aria-describedby="Message" value="<?= $datas != false ? $datas->bookMessage : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->