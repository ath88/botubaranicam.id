<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>


	<!--  Essential META Tags -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="botubaranicam.id" content="Whale Sighting at BotuBarani, Bone Bolango, Indonesia." />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic|Play+Fair:700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/style.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/swiper.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/components/select-boxes.css" type="text/css" />

	<link rel="stylesheet" href="<?= base_url() ?>assets/css/components/bs-rating.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<!-- Primary Meta Tags -->
	<title>Whale Shark Sighting at BotuBarani, Bone Bolango, Indonesia.</title>
	<meta name="title" content="Whale Shark Sighting at BotuBarani, Bone Bolango, Indonesia.">
	<meta name="description" content="Offering special interest botubarani whale shark tours for individuals or groups">

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://botubaranicam.id/">
	<meta property="og:title" content="Whale Shark Sighting at BotuBarani, Bone Bolango, Indonesia.">
	<meta property="og:description" content="Offering special interest botubarani whale shark tours for individuals or groups">
	<meta property="og:image" content="https://botubaranicam.id/assets/images/virsal/virsal.jpg">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:url" content="https://botubaranicam.id/">
	<meta property="twitter:title" content="Whale Shark Sighting at BotuBarani, Bone Bolango, Indonesia.">
	<meta property="twitter:description" content="Offering special interest botubarani whale shark tours for individuals or groups">
	<meta property="twitter:image" content="https://botubaranicam.id/assets/images/virsal/virsal.jpg">

</head>

<body class="stretched sticky-responsive-pagemenu">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="<?= base_url() ?>" class="standard-logo" data-dark-logo="assets/images/virsal.png"><img src="assets/images/virsal.png" alt="Virsal Logo"></a>
						<a href="<?= base_url() ?>" class="retina-logo" data-dark-logo="assets/images/virsal@2x.png"><img src="assets/images/virsal@2x.png" alt="Virsal Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul id="logo_partner">
							<a href="https://brin.go.id/" target="_blank" class="standard-logo"><img src="assets/images/virsal/partner/brin.png" alt="BRIN Logo"></a>&nbsp;&nbsp;
							<a href="https://desaberinovasi.brin.go.id/" target="_blank" class="standard-logo"><img src="assets/images/virsal/partner/desa_berinovasi.png" alt="Desa Berinovasi BRIN Logo"></a>&nbsp;&nbsp;
							<a href="https://yekhali.org/" target="_blank" class="standard-logo"><img src="assets/images/virsal/partner/yekhali.png" alt="YekHali Logo"></a>&nbsp;&nbsp;
							<a href="https://ilmukelautan.fp.unram.ac.id/" target="_blank" class="standard-logo"><img src="assets/images/virsal/partner/ilmu_kelautan_unram.png" alt="Ilmu Kelautan UnRam Logo"></a>
						</ul>
						<!-- Top Search
						============================================= -->
						<div id="top-search">
							<a href="<?= base_url() ?>login"><i class="icon-enter"></i><i class="icon-line-cross"></i></a>
						</div><!-- #top-search end -->

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

		<section id="slider" class="boxed-slider">
			<div class="heading-block center">
				<h2>WHALE SHARK SIGHTING</h2>
				<span>BOTUBARANI, BONE BOLANGO, INDONESIA.</span>
			</div>
			<?php
			if ($slider != false) :
				$i = 1;
				foreach ($slider as $row) :
			?>
					<div class="container clearfix">
						<div class="heading-block center">
							<h2><?= $row->camCaption ?></h2>
							<div class="widget">
								<div class="left_half">
									<input id="rating-<?= $i++ ?>" data-id="<?= $row->camId ?>" type="number" class="rating" data-size="sm" min="1" max="5" data-step="1" value="<?= number_format($row->rateValue, 1) ?>" data-glyphicon="false" data-rating-class="fontawesome-icon">
								</div>
								<div class="right_half col_last">
									<a href="#myModal1" data-lightbox="inline" class="button button-small button-rounded">Comment Our Video</a>
								</div>
							</div>
						</div>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/<?= youtubeid($row->camSource) ?>" title="<?= $row->camCaption ?>" frameborder="0" allow="accelerometer;  encrypted-media; gyroscope; picture-in-picture" mozallowfullscreen webkitallowfullscreen allowfullscreen></iframe>

					</div>

					<div class="container clearfix">&nbsp;</div>
			<?php
				endforeach;
			endif;

			?>

		</section>

		<!-- Page Sub Menu
		============================================= -->
		<div id="page-menu">

			<div id="page-menu-wrap">

				<div class="container clearfix">

					<div class="menu-title">Explore <span>VIRSAL</span></div>

					<nav class="one-page-menu">
						<ul>
							<li><a href="#" data-href="#header">
									<div>Start</div>
								</a></li>
							<li><a href="#" data-href="#section-work">
									<div>Highlight</div>
								</a></li>
							<li><a href="#" data-href="#section-booking">
									<div>Booking</div>
								</a></li>
							<li><a href="#" data-href="#section-package">
									<div>Package</div>
								</a></li>
							<li><a href="#" data-href="#section-whales">
									<div>Whale Shark</div>
								</a></li>
							<li><a href="#" data-href="#section-teams">
									<div>Guided</div>
								</a></li>
							<li><a href="#" data-href="#section-weather">
									<div>Live Weather</div>
								</a></li>
						</ul>
					</nav>

					<div id="page-submenu-trigger"><i class="icon-reorder"></i></div>

				</div>

			</div>

		</div><!-- #page-menu end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<section id="section-work" class="page-section">

					<div class="heading-block center">
						<h2>Highlight <span>Footage</span></h2>
						<span>Some of the Awesome Sighting Whale Shark.</span>
					</div>

					<div class="container clearfix center">

						<!-- Portfolio Items
						============================================= -->
						<div id="portfolio" class="portfolio portfolio-nomargin clearfix">

							<?php
							if ($highlight != false) :
								foreach ($highlight as $row) :
							?>
									<article class="portfolio-item">
										<div class="portfolio-image">
											<a href="javascript:void(0);">
												<img src="https://img.youtube.com/vi/<?= youtubeid($row->hlSource) ?>/hqdefault.jpg" alt="<?= $row->hlTitle ?>">
											</a>
											<div class="portfolio-overlay">
												<a href="<?= $row->hlSource ?>" class="center-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
											</div>
										</div>
									</article>
							<?php
								endforeach;
							endif;
							?>
						</div><!-- #portfolio end -->

					</div>

				</section>

				<section id="section-booking" class="page-section section parallax dark" style="padding: 200px 0;" data-stellar-background-ratio="0.3">

					<div class="container clearfix">

						<div class="fancy-title title-dotted-border">
							<h3>Book our package</h3>
						</div>

						<div class="contact-widget">

							<div class="contact-form-result"></div>

							<form class="nobottommargin" id="template-contactform" name="template-contactform" action="frontpage/booking" method="post" novalidate="novalidate">

								<div class="form-process"></div>

								<div class="col_one_third">
									<label for="template-contactform-name">Name <small>*</small></label>
									<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" aria-required="true">
								</div>

								<div class="col_one_third">
									<label for="template-contactform-email">Email <small>*</small></label>
									<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" aria-required="true">
								</div>

								<div class="col_one_third col_last">
									<label for="template-contactform-phone">Number/Whatapps/Telegram</label>
									<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="required sm-form-control">
								</div>

								<div class="col_half">
									<label for="template-contactform-package">Package <small>*</small></label>
									<select class="select-1 sm-form-control required" style="width:100%;" id="template-contactform-package" name="template-contactform-package">
										<option value="">Select Package</option>
										<?php
										if ($package != false) :
											foreach ($package as $row) :
												echo '<option value="' . $row->packId . '">' . $row->packName . ' | IDR ' . number_format($row->packPrice, 0) . 'K</option>';
											endforeach;
										endif;
										?>
									</select>
								</div>

								<div class="col_half col_last">
									<label for="template-contactform-guide">Our Guide</label>
									<select class="select-1 sm-form-control required" style="width:100%;" id="template-contactform-guide" name="template-contactform-guide">
										<option value="">Select Guide</option>
										<?php
										if ($guided != false) :
											foreach ($guided as $row) :
												echo '<option value="' . $row->guideId . '">' . $row->guideName . '</option>';
											endforeach;
										endif;
										?>
									</select>
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-contactform-message">Message <small>*</small></label>
									<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30" aria-required="true"></textarea>
								</div>

								<div class="col_full hidden">
									<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control">
								</div>

								<div class="col_full">
									<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
								</div>

							</form>

						</div>

					</div>

				</section>

				<section id="section-package" class="page-section topmargin-lg">

					<div class="container clearfix">

						<div class="heading-block center">
							<h2>Tour <span>Package</span></h2>
						</div>

						<div class="pricing bottommargin clearfix">
							<?php
							$animate = ['fadeInLeft', 'fadeInDown', 'fadeInUp', 'fadeInRight'];
							$delay = [250, 500, 750];
							if ($package != false) :
								$i = 0;
								foreach ($package as $row) :
							?>
									<div class="col-md-3" data-animate="<?= $animate[$i] ?>" data-delay="<?= $delay[rand(0, 2)] ?>">

										<div class="pricing-box <?= $row->packStar == 1 ? 'best-price' : '' ?>">
											<div class="pricing-title">
												<h3><?= $row->packName ?></h3>
											</div>
											<div class="pricing-price">
												<span class="price-unit">IDR</span><?= number_format($row->packPrice, 0) ?><span class="price-tenure">K</span>
											</div>
											<div class="pricing-features">
												<ul>
													<li><?= $row->packFitur1 ?></li>
													<li><?= $row->packFitur2 ?></li>
													<li><?= $row->packFitur3 ?></li>
													<li><?= $row->packFitur4 ?></li>
													<li><?= $row->packFitur5 ?></li>
													<li><?= $row->packStar == 1 ? '<i class="icon-star3">' : '' ?></i>
														<?= $row->packStar == 1 ? '<i class="icon-star3">' : '' ?></i>
														<?= $row->packStar == 1 ? '<i class="icon-star3">' : '' ?></i>
														<?= $row->packStar == 1 ? '<i class="icon-star3">' : '' ?></i>
														<?= $row->packStar == 1 ? '<i class="icon-star3">' : '' ?></i>
													</li>
												</ul>
											</div>
											<div class="pricing-action">
												<a href="#" class="btn btn-danger btn-block btn-lg <?= $row->packStar == 1 ? 'bgcolor border-color' : '' ?>">Book Now</a>
											</div>
										</div>

									</div>
							<?php
									$i++;
								endforeach;
							endif;
							?>

						</div>

						<div class="clear"></div>

					</div>

				</section>

				<section id="section-whales" class="page-section section parallax dark" style="padding: 80px 0;" data-stellar-background-ratio="0.3">

					<div class="container clearfix">
						<div class="heading-block center">
							<h2>Whale Shark <span>Graph</span></h2>
						</div>
						<div class="section dark parallax nobottommargin">

							<div class="container clearfix">

								<div class="divcenter" style="min-height: 150px;">
									<canvas id="chart-0"></canvas>
								</div>

							</div>

						</div>

					</div>

				</section>

				<section id="section-teams" class="page-section topmargin-lg">

					<div class="heading-block center">
						<h2>Our Guide</h2>
						<span>People who have contributed enormously to botubarani.</span>
					</div>

					<div class="container clearfix">
						<?php
						if ($guided != false) :
							foreach ($guided as $row) :
						?>
								<div class="col-md-3 col-sm-6 bottommargin">

									<div class="team">
										<div class="team-image">
											<img src="<?= $row->guidePhoto ?>" alt="John Doe">
										</div>
										<div class="team-desc">
											<div class="team-title">
												<h4><?= $row->guideName ?></h4><span><?= $row->guideFitur1 ?>&nbsp;&nbsp;<i class="icon-star">&nbsp;<strong><?= $row->guideRating ?> / 5.0</strong></i></span>
											</div>
											<a href="<?= $row->guideFB ?>" class="social-icon inline-block si-small si-light si-rounded si-facebook">
												<i class="icon-facebook"></i>
												<i class="icon-facebook"></i>
											</a>
											<a href="<?= $row->guideInstagram ?>" class="social-icon inline-block si-small si-light si-rounded si-instagram">
												<i class="icon-instagram"></i>
												<i class="icon-instagram"></i>
											</a>
											<a href="<?= $row->guideTwitter ?>" class="social-icon inline-block si-small si-light si-rounded si-twitter">
												<i class="icon-twitter"></i>
												<i class="icon-twitter"></i>
											</a>
										</div>
									</div>

								</div>
						<?php
							endforeach;
						endif; ?>
					</div>

				</section>

				<section id="section-weather" class="page-section section parallax dark" style="padding: 80px 0;" data-stellar-background-ratio="0.3">

					<div class="container clearfix">

						<a class="weatherwidget-io" href="https://forecast7.com/en/0d70122d45/gorontalo/" data-label_1="GORONTALO" data-label_2="WEATHER" data-textcolor="#ffffff">GORONTALO WEATHER</a>
						<script>
							! function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (!d.getElementById(id)) {
									js = d.createElement(s);
									js.id = id;
									js.src = 'https://weatherwidget.io/js/widget.min.js';
									fjs.parentNode.insertBefore(js, fjs);
								}
							}(document, 'script', 'weatherwidget-io-js');
						</script>
				</section>

			</div>

		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">

					<div class="col_two_third">

						<div class="col_one_third">

							<div class="widget clearfix">

								<h3>botubaranicam.id</h3>

								<p>WHALE SHARK SPECIAL INTEREST TOUR.</p>

								<div style="background: url('assets/images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>Headquarters:</strong><br>
										Desa Botubarani, <br>
										Kecamatan Kabila Bone, <br />
										Kabupaten Bone Bolango, Gorontalo<br>
									</address>
									<abbr title="Phone Number"><strong>Phone 1:</strong></abbr> (62) 821 1329 4603 <br>
									<abbr title="Phone Number"><strong>Phone 2:</strong></abbr> (62) 852 4009 0354<br>
									<abbr title="Email Address"><strong>Email:</strong></abbr> mahardika@unram.ac.id
								</div>

							</div>

							<!-- <div class="widget clearfix">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.68142626926!2d123.0989604147533!3d0.47462939965372397!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x327f2b1ebb06a7ad%3A0x6e207bf0624d792f!2sWisata%20Hiu%20Paus%20Botubarani!5e0!3m2!1sen!2sid!4v1635557693343!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
							</div> -->

						</div>

						<div class="col_one_third">

							<div class="widget widget_links clearfix">

								<h3>Partner</h3>

								<ul>
									<?php if ($partner != false) :
										foreach ($partner as $row) :
									?>
											<li><a href="<?= $row->partnerURL ?>" title="<?= $row->partnerCaption ?>"><?= $row->partnerTitle ?></a></li>
									<?php endforeach;
									endif; ?>
								</ul>

							</div>

						</div>

						<div class="col_one_third col_last">
							<div class="widget clearfix">
								<h3 style="margin:0;">DONATION HELP</h3>
								<h3 style="font-size: 21px;"><span><strong>SAVE WHALES SHARK</strong></span></h3>
								<h5 style="font-size: 13px;">donate now in the PayPal link below:</h5>
								<div class="widget-subscribe-form-result"></div>
								<a href="https://www.paypal.com/donate/?token=HSnGCXo2oKUc52AUGaY9nvyoE2ExQvmxgFW9VfOHB7H_jGTgxRcSYhPBf8JFK_b54N40L_p45tlPFTuR" target="_blank">
									<img src="assets/images/virsal/paypal_donation.png" width="195px">
								</a>
							</div>
						</div>

					</div>

					<div class="col_one_third col_last">

						<div class="widget clearfix">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.68142626926!2d123.0989604147533!3d0.47462939965372397!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x327f2b1ebb06a7ad%3A0x6e207bf0624d792f!2sWisata%20Hiu%20Paus%20Botubarani!5e0!3m2!1sen!2sid!4v1635557693343!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
							<!-- <h3>DONATION HELP <br /><span>SAVE WHALES SHARK</span></h3>
							<h5>donate now in the PayPal link below:</h5>
							<div class="widget-subscribe-form-result"></div>
							<a href="https://www.paypal.com/donate/?token=HSnGCXo2oKUc52AUGaY9nvyoE2ExQvmxgFW9VfOHB7H_jGTgxRcSYhPBf8JFK_b54N40L_p45tlPFTuR" target="_blank">
								<img src="assets/images/virsal/paypal_donation.png" width="235px">
							</a> -->
						</div>
					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

						Copyrights &copy; 2021 All Rights Reserved by botubaranicam.id

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- <div class="modal-on-load" data-target="#myModal1"></div> -->

	<!-- Modal -->
	<div class="modal1 mfp-hide" id="myModal1">
		<div class="block divcenter" style="background-color: #FFF; max-width: 80%; padding: 10px;">
			<div id="disqus_thread"></div>
			<script>
				/**
				 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
				 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
				/*
				var disqus_config = function () {
				this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
				this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
				};
				*/
				(function() { // DON'T EDIT BELOW THIS LINE
					var d = document,
						s = d.createElement('script');
					s.src = 'https://botubarani.disqus.com/embed.js';
					s.setAttribute('data-timestamp', +new Date());
					(d.head || d.body).appendChild(s);
				})();
			</script>
			<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
		</div>
	</div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?= base_url() ?>assets/js/functions.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/components/star-rating.js"></script>

	<script type="text/javascript" src="<?= base_url() ?>assets/js/chart.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/chart-utils.js"></script>
	<script type="text/javascript" src="assets/js/components/select-boxes.js"></script>

	<script type="text/javascript">
		var config = {
			type: 'bar',
			data: {
				labels: <?= json_encode($w_month) ?>,
				datasets: [{
					label: "Whale Shark Sighting",
					backgroundColor: window.chartColors.green,
					borderColor: window.chartColors.green,
					data: <?= json_encode($w_whales) ?>,
					fill: false,
				}]
			},
			options: {
				responsive: true,
				legend: {
					display: false
				},
				title: {
					fontColor: 'white',
					display: false,
					text: 'Whale Sighting In Graph'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					fontColor: 'white',
					xAxes: [{
						ticks: {
							fontColor: 'white',
						},
						display: true,
						scaleLabel: {
							display: true,
							fontColor: 'white',
							fontSize: 18,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						ticks: {
							fontColor: 'white',
						},
						display: true,
						scaleLabel: {
							display: true,
							fontColor: 'white',
							fontSize: 18,
							labelString: 'Whale Shark'
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById("chart-0").getContext("2d");
			window.myLine = new Chart(ctx, config);
		};
	</script>

	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script type="text/javascript">
		function ajax_rating(value, id) {
			$.ajax({
				url: "/frontpage/rating",
				data: "value=" + value + "&id=" + id,
				method: 'post',
				success: function(data) {
					var res = $.parseJSON(data);
					swal.fire("Rating!", res.message, res.status).then(function() {
						location.reload();
					});
				}
			});
		}
		$("#rating-1,#rating-2,#rating-3,#rating-4,#rating-5").on("rating.change", function(event, value, caption) {
			ajax_rating(value, $(this).data("id"))
		});

		$("#template-contactform").submit(function(e) {
			e.preventDefault();
			$.ajax({
				type: $(e).attr('method'),
				url: $(e).attr('action'),
				data: $(e).serialize(),
				success: function(data) {
					var res = $.parseJSON(data);
					swal.fire("Rating!", res.message, res.status).then(function() {
						location.reload();
					});
				}
			})
		})
	</script>
</body>

</html>