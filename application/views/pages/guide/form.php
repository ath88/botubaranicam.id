<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="guideIdOld" value="<?= $datas != false ? $datas->guideId : '' ?>">

                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="guideName" placeholder="Name" aria-describedby="Name" value="<?= $datas != false ? $datas->guideName : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Photo URL</label>
                            <input type="text" class="form-control" name="guidePhoto" placeholder="Photo URL" aria-describedby="Photo URL" value="<?= $datas != false ? $datas->guidePhoto : '' ?>">
                            <span class="form-text text-muted">Ex. https://www.youtube.com/watch?v=K2GhkPi3kMU</span>
                        </div>

                        <div class="form-group">
                            <label>Rating</label>
                            <input type="text" class="form-control" name="guideRating" placeholder="Rating" aria-describedby="Rating" value="<?= $datas != false ? $datas->guideRating : '' ?>">
                            <span class="form-text text-muted">Ex. 0.0 - 5.0</span>
                        </div>

                        <div class="form-group">
                            <label>Profile FB URL</label>
                            <input type="text" class="form-control" name="guideFB" placeholder="Profile FB URL" aria-describedby="Profile FB URL" value="<?= $datas != false ? $datas->guideFB : '' ?>">
                            <span class="form-text text-muted">Ex. https://www.facebook.com/agus.tri.haryono</span>
                        </div>

                        <div class="form-group">
                            <label>Profile Instagram URL</label>
                            <input type="text" class="form-control" name="guideInstagram" placeholder="Profile Instagram URL" aria-describedby="Profile Instagram URL" value="<?= $datas != false ? $datas->guideInstagram : '' ?>">
                            <span class="form-text text-muted">Ex. https://www.instagram.com/agustharyono24/</span>
                        </div>

                        <div class="form-group">
                            <label>Profile Twitter URL</label>
                            <input type="text" class="form-control" name="guideTwitter" placeholder="Profile Twitter URL" aria-describedby="Profile Twitter URL" value="<?= $datas != false ? $datas->guideTwitter : '' ?>">
                            <span class="form-text text-muted">Ex. https://twitter.com/agustharyono</span> 
                        </div>

                        <div class="form-group">
                            <label>Caption 1</label>
                            <input type="text" class="form-control" name="guideFitur1" placeholder="Caption 1" aria-describedby="Caption 1" value="<?= $datas != false ? $datas->guideFitur1 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Caption 2</label>
                            <input type="text" class="form-control" name="guideFitur2" placeholder="Caption 2" aria-describedby="Caption 2" value="<?= $datas != false ? $datas->guideFitur2 : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->