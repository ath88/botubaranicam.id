<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="packIdOld" value="<?= $datas != false ? $datas->packId : '' ?>">

                        <div class="form-group">
                            <label>Package Name</label>
                            <input type="text" class="form-control" name="packName" placeholder="Package Name" aria-describedby="Package Name" value="<?= $datas != false ? $datas->packName : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Price</label>
                            <input type="text" class="form-control" name="packPrice" placeholder="Price" aria-describedby="Price" value="<?= $datas != false ? $datas->packPrice : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Feature 1</label>
                            <input type="text" class="form-control" name="packFitur1" placeholder="Feature 1" aria-describedby="Feature 1" value="<?= $datas != false ? $datas->packFitur1 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Feature 2</label>
                            <input type="text" class="form-control" name="packFitur2" placeholder="Feature 2" aria-describedby="Feature 2" value="<?= $datas != false ? $datas->packFitur2 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Feature 3</label>
                            <input type="text" class="form-control" name="packFitur3" placeholder="Feature 3" aria-describedby="Feature 3" value="<?= $datas != false ? $datas->packFitur3 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Feature 4</label>
                            <input type="text" class="form-control" name="packFitur4" placeholder="Feature 4" aria-describedby="Feature 4" value="<?= $datas != false ? $datas->packFitur4 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Feature 5</label>
                            <input type="text" class="form-control" name="packFitur5" placeholder="Feature 5" aria-describedby="Feature 5" value="<?= $datas != false ? $datas->packFitur5 : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->